<?php

namespace asmaru\http;

use function file_get_contents;
use function is_array;
use function json_decode;
use function mb_strtolower;
use function parse_url;
use function strpos;
use const PHP_URL_PATH;

/**
 * Class RequestFactory
 *
 * @package asmaru\http
 */
class RequestFactory {

	/**
	 * Builds a request object from current request parameters.
	 *
	 * @return Request
	 */
	public function build(): Request {
		return new Request($this->getMethod(), $this->getPath(), $this->getBase(), $this->getParameters(), $_SERVER, $this->getFiles());
	}

	/**
	 * @return string
	 */
	private function getMethod() {
		$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
		if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])) {
			$method = $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'];
		}
		if (isset($_REQUEST['HTTP_X_HTTP_METHOD_OVERRIDE'])) {
			$method = $_REQUEST['HTTP_X_HTTP_METHOD_OVERRIDE'];
		}
		return $method;
	}

	/**
	 * @return string
	 */
	private function getBase(): string {
		return isset($_SERVER['BASEPATH']) ? $_SERVER['BASEPATH'] : '/';
	}

	/**
	 * @return string
	 */
	private function getPath(): Path {
		if (!isset($_SERVER['REQUEST_URI'])) return new Path('');
		$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		return new Path(mb_substr($path, mb_strlen($this->getBase())));
	}

	/**
	 * @return array
	 */
	private function getFiles() {
		$files = [];
		foreach ($_FILES as $name => $file) {
			if (is_array($file['name'])) {
				$files[$name] = [];
				for ($i = 0; $i < count($file['name']); $i++) {
					$files[$name][] = [
						'name' => $file['name'][$i],
						'type' => $file['type'][$i],
						'tmp_name' => $file['tmp_name'][$i],
						'error' => $file['error'][$i],
						'size' => $file['size'][$i]
					];
				}
			} else {
				$files[$name] = [$file];
			}
		}
		return $files;
	}

	/**
	 * @return array
	 */
	private function getParameters(): array {
		if (isset($_SERVER['CONTENT_TYPE'])) {
			$contentType = mb_strtolower($_SERVER['CONTENT_TYPE']);
			if (strpos($contentType, 'application/json') === 0) {
				$input = file_get_contents('php://input');
				if (!empty($input)) {
					$data = json_decode($input, true);
					if (is_array($data)) {
						return $data;
					}
				}
			}
		}
		if (!empty($_REQUEST)) {
			return $_REQUEST;
		}
		return [];
	}
}