<?php

namespace asmaru\http;

/**
 * Interface Filter
 *
 * @package asmaru\http
 */
interface Filter {

	/**
	 * @param Request $request
	 * @param Response $response
	 */
	public function filter(Request $request, Response $response);
}