<?php

namespace asmaru\http;

use asmaru\di\ObjectManager;
use asmaru\http\error\HttpError;
use Closure;
use InvalidArgumentException;
use ReflectionClass;
use function urldecode;

class App {

	public ?Request $request = null;

	public Route $currentRoute;

	public ObjectManager $objectManager;

	private array $routes = [];

	private array $filters = [];

	private RequestHandler $defaultRoute;

	/**
	 * App constructor.
	 *
	 * @param Request $request
	 * @param ObjectManager $objectManager
	 */
	public function __construct(Request $request, ObjectManager $objectManager) {
		$this->request = $request;
		$this->objectManager = $objectManager;
	}

	/**
	 * @return Response
	 */
	public function run() {

		try {

			/** @var Route $route */
			foreach ($this->filters as $route) {
				if ($route->match($this->request->method, new Path($this->request->path))) {
					$filterResponse = $this->executeRouteAction($route->action);
					if (!empty($filterResponse) && $filterResponse instanceof Response) {
						return $filterResponse;
					}
				}
			}

			$response = null;

			/** @var Route $action */
			foreach ($this->routes as $route) {
				if ($route->match($this->request->method, new Path($this->request->path))) {
					$this->currentRoute =& $route;
					foreach ($this->currentRoute->values as $key => $value) {
						$this->request->set(':' . $key, urldecode($value));
					}
					$response = $this->executeRouteAction($route->action);
					break;
				}
			}

			if (empty($response) && $this->defaultRoute !== null) {
				$response = $this->executeRouteAction($this->defaultRoute);
			}

			if (!empty($response)) {
				if ($response instanceof Response) {
					return $response;
				}
				return (new Response())->setContent($response);
			}

			return (new Response())->status(HttpStatus::HTTP_NOT_FOUND);
		} /** @noinspection PhpRedundantCatchClauseInspection */ catch (HttpError $e) {
			return (new Response())->status($e->getStatus())->json(['message' => $e->getMessage()]);
		}
	}

	/**
	 * @param $pattern
	 * @param $action
	 *
	 * @throws InvalidArgumentException
	 */
	public function filter($pattern, $action) {
		$this->filters[] = $this->createAction($pattern, $action);
	}

	/**
	 * @param $pattern
	 * @param $action
	 *
	 * @return Route
	 * @throws InvalidArgumentException
	 */
	private function createAction($pattern, $action) {
		if (preg_match('/^([\S]+)\s([\S]+)(?:\sas\s(.*))?$/', mb_strtolower($pattern), $matches) === 1) {
			$methods = $matches[1] === '*' ? ['POST', 'PUT', 'DELETE', 'GET'] : explode('|', mb_strtoupper($matches[1]));
			$path = new Path($matches[2]);
			$name = isset($matches[3]) ? $matches[3] : null;
			return new Route($path, $action, $methods, $name);
		}
		throw new InvalidArgumentException('invalid route pattern');
	}

	/**
	 * @param $action
	 */
	public function defaultRoute($action) {
		$this->defaultRoute = $action;
	}

	/**
	 * @param $pattern
	 * @param $action
	 */
	public function route($pattern, $action): void {
		$this->routes[] = $this->createAction($pattern, $action);
	}

	/**
	 * @param $name
	 *
	 * @return Route
	 */
	public function getRoute($name) {
		/** @var Route $route */
		foreach ($this->routes as $route) {
			if ($route->name === mb_strtolower($name)) {
				return $route;
			}
		}
	}

	/**
	 * @param $action
	 *
	 * @return Response|mixed
	 */
	private function executeRouteAction($action) {
		if ($action instanceof Closure) {
			return $this->objectManager->call($action);
		}

		if (!$action instanceof RequestHandler) {
			$reflectionClass = new ReflectionClass($action);
			if (!$reflectionClass->implementsInterface(RequestHandler::class)) {
				throw new InvalidArgumentException(sprintf('action must implement "%s"', RequestHandler::class));
			}
			$action = $this->objectManager->make($action);
		}
		return $action->execute();
	}
}