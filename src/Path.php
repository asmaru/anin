<?php

declare(strict_types=1);

namespace asmaru\http;

use Countable;
use function array_filter;
use function explode;
use function implode;

class Path implements Countable {

	const PATH_SEPARATOR = '/';

	private $parts;

	public function __construct(string $path) {
		$this->parts = $this->normalize($path);
	}

	public static function fromArray(array $parts) {
		return new static(implode(static::PATH_SEPARATOR, $parts));
	}

	public function toArray(): array {
		return $this->parts;
	}

	public function __toString(): string {
		return implode(self::PATH_SEPARATOR, $this->parts);
	}

	public function firstSegment(): string {
		return $this->parts[0];
	}

	public function lastSegment(): string {
		return $this->parts[count($this->parts) - 1];
	}

	private function normalize(string $path): array {
		return array_filter(explode(self::PATH_SEPARATOR, trim(mb_strtolower($path))));
	}

	public function count(): int {
		return count($this->parts);
	}
}