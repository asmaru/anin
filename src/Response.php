<?php

namespace asmaru\http;

use finfo;
use function filectime;
use function json_encode;
use function sprintf;

class Response {

	const HTTP_CONTINUE = 100;

	const HTTP_SWITCHING_PROTOCOLS = 101;

	const HTTP_PROCESSING = 102;            // RFC2518

	const HTTP_OK = 200;

	const HTTP_CREATED = 201;

	const HTTP_ACCEPTED = 202;

	const HTTP_NON_AUTHORITATIVE_INFORMATION = 203;

	const HTTP_NO_CONTENT = 204;

	const HTTP_RESET_CONTENT = 205;

	const HTTP_PARTIAL_CONTENT = 206;

	const HTTP_MULTI_STATUS = 207;          // RFC4918

	const HTTP_ALREADY_REPORTED = 208;      // RFC5842

	const HTTP_IM_USED = 226;               // RFC3229

	const HTTP_MULTIPLE_CHOICES = 300;

	const HTTP_MOVED_PERMANENTLY = 301;

	const HTTP_FOUND = 302;

	const HTTP_SEE_OTHER = 303;

	const HTTP_NOT_MODIFIED = 304;

	const HTTP_USE_PROXY = 305;

	const HTTP_RESERVED = 306;

	const HTTP_TEMPORARY_REDIRECT = 307;

	const HTTP_PERMANENTLY_REDIRECT = 308;  // RFC7238

	const HTTP_BAD_REQUEST = 400;

	const HTTP_UNAUTHORIZED = 401;

	const HTTP_PAYMENT_REQUIRED = 402;

	const HTTP_FORBIDDEN = 403;

	const HTTP_NOT_FOUND = 404;

	const HTTP_METHOD_NOT_ALLOWED = 405;

	const HTTP_NOT_ACCEPTABLE = 406;

	const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;

	const HTTP_REQUEST_TIMEOUT = 408;

	const HTTP_CONFLICT = 409;

	const HTTP_GONE = 410;

	const HTTP_LENGTH_REQUIRED = 411;

	const HTTP_PRECONDITION_FAILED = 412;

	const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;

	const HTTP_REQUEST_URI_TOO_LONG = 414;

	const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;

	const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;

	const HTTP_EXPECTATION_FAILED = 417;

	const HTTP_I_AM_A_TEAPOT = 418;

	const HTTP_UNPROCESSABLE_ENTITY = 422;

	const HTTP_LOCKED = 423;

	const HTTP_FAILED_DEPENDENCY = 424;

	const HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;

	const HTTP_UPGRADE_REQUIRED = 426;

	const HTTP_PRECONDITION_REQUIRED = 428;

	const HTTP_TOO_MANY_REQUESTS = 429;

	const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;

	const HTTP_INTERNAL_SERVER_ERROR = 500;

	const HTTP_NOT_IMPLEMENTED = 501;

	const HTTP_BAD_GATEWAY = 502;

	const HTTP_SERVICE_UNAVAILABLE = 503;

	const HTTP_GATEWAY_TIMEOUT = 504;

	const HTTP_VERSION_NOT_SUPPORTED = 505;

	const HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL = 506;

	const HTTP_INSUFFICIENT_STORAGE = 507;

	const HTTP_LOOP_DETECTED = 508;

	const HTTP_NOT_EXTENDED = 510;

	const HTTP_NETWORK_AUTHENTICATION_REQUIRED = 511;

	public int $status = HttpStatus::HTTP_OK;

	public string $contentType = ContentType::HTML;

	public mixed $content;

	private string $location;

	private string $file;

	private string $downloadFilename;

	private array $headers = [];

	public function __construct($content = null) {
		$this->content = $content;
	}

	public function redirect(string $location, int $status = HttpStatus::HTTP_FOUND): static {
		$this->location = $location;
		$this->status = $status;
		return $this;
	}

	public function send(): Response {

		foreach ($this->headers as $header) {
			header($header);
		}

		http_response_code($this->status);

		// Handle redirect
		if (!empty($this->location)) {
			header('Location: ' . $this->location);
			return $this;
		}

		header('Content-Type: ' . $this->contentType);

		// Handle file download
		if (!empty($this->file)) {
			header('etag:' . filectime($this->file));
			if (!empty($this->downloadFilename)) {
				header('Content-Disposition: attachment; filename="' . $this->downloadFilename . '"');
			}
			readfile($this->file);
			return $this;
		}

		// Return content
		echo $this->content;
		return $this;
	}

	public function status(int $status): Response {
		$this->status = $status;
		return $this;
	}

	public function json(mixed $json, int $status = HttpStatus::HTTP_OK): Response {
		$this->status = $status;
		return $this->setContent(json_encode($json), ContentType::JSON, $status);
	}

	public function setContent(string $content, string $contentType = ContentType::HTML, int $status = HttpStatus::HTTP_OK): Response {
		$this->status = $status;
		$this->content = $content;
		$this->contentType = $contentType;
		return $this;
	}

	public function auth(): Response {
		$this->status = HttpStatus::HTTP_UNAUTHORIZED;
		$this->headers[] = 'www-authenticate: Basic realm=""';
		return $this;
	}

	public function withHeader(string $name, string $value): Response {
		$this->headers[$name] = sprintf('%s: %s', $name, $value);
		return $this;
	}

	public function setFile(string $file, string $downloadFilename = null): Response {
		$info = new finfo(FILEINFO_MIME_TYPE);
		$this->contentType = $info->file($file);
		$this->file = $file;
		$this->downloadFilename = $downloadFilename;
		return $this;
	}

	public function headers(): array {
		return $this->headers;
	}

	public static function internalError(): Response {
		return (new static())->status(HttpStatus::HTTP_INTERNAL_SERVER_ERROR);
	}

	public static function created(): Response {
		return (new static())->status(HttpStatus::HTTP_CREATED);
	}

	public static function ok(): Response {
		return (new static())->status(HttpStatus::HTTP_OK);
	}
}