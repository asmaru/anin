<?php

namespace asmaru\http;

interface ContentType {

	const JSON = 'application/json; charset=utf-8';

	const HTML = 'text/html; charset=utf-8';
}