<?php
/**
 * Copyright (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

namespace asmaru\http;

use Closure;

class RequestHandlerClosureWrapper {

	/** @var Closure */
	private $callback;

	/**
	 * @param Closure $callback
	 */
	public function __construct(Closure $callback) {
		$this->callback = $callback;
	}

	/**
	 * @param App $app
	 * @return Response
	 */
	public function execute(App $app) {
		return $app->objectManager->call($this->callback);
	}
}