<?php

namespace asmaru\http;

/**
 * Class Request
 *
 * @package asmaru\http
 */
class Request {

	/**
	 * @var string
	 */
	public $method;

	/**
	 * @var Path
	 */
	public $path;

	/**
	 * @var string
	 */
	public $baseURL;

	/**
	 * @var array
	 */
	private $parameters;

	/**
	 * @var array
	 */
	private $server;

	/**
	 * @var array
	 */
	private $files;

	/**
	 * Request constructor.
	 *
	 * @param string $method
	 * @param $path
	 * @param string $base
	 * @param array $parameters
	 * @param array $server
	 * @param array $files
	 */
	public function __construct(string $method, Path $path, string $base = '', array $parameters = [], array $server = [], array $files = []) {
		$this->parameters = $parameters;
		$this->path = $path;
		$this->method = mb_strtolower($method);
		$this->server = $server;
		$this->baseURL = $this->getBaseURL() . $base;
		$this->files = $files;
	}

	/**
	 * @return string
	 */
	private function getBaseURL(): string {
		$isHTTPS = $this->isHTTPS();
		$protocol = mb_strtolower($this->env('SERVER_PROTOCOL', 'HTTP/1.1'));
		$protocol = mb_substr($protocol, 0, mb_strpos($protocol, '/')) . (($isHTTPS) ? 's' : '');
		$port = $this->env('SERVER_PORT', 80);
		$port = ((!$isHTTPS && $port == 80) || ($isHTTPS && $port == 443)) ? '' : ':' . $port;
		$host = $this->env('HTTP_X_FORWARDED_HOST') !== null ? $this->env('HTTP_X_FORWARDED_HOST') : $this->env('HTTP_HOST');
		$host = isset($host) ? $host : $this->env('SERVER_NAME') . $port;
		return $protocol . '://' . $host;
	}

	/**
	 * @return bool
	 */
	public function isHTTPS(): bool {
		return (!empty($this->env('HTTPS')) && $this->env('HTTPS') !== 'off') || $this->env('SERVER_PORT') == 443;
	}

	/**
	 * @param string $key
	 * @param string $default
	 * @return string
	 */
	public function env(string $key, $default = null) {
		return isset($this->server[$key]) ? $this->server[$key] : $default;
	}

	/**
	 * @param string $key
	 * @return array
	 */
	public function files($key = null): array {
		return $key !== null && isset($this->files[$key]) ? $this->files[$key] : $this->files;
	}

	/**
	 * @param string $method
	 * @return bool
	 */
	public function is(string $method): bool {
		return mb_strtolower($method) === $this->method;
	}

	/**
	 * @param string $key
	 * @return bool
	 */
	public function contains(string $key): bool {
		return isset($this->parameters[$key]);
	}

	/**
	 * @param string $name
	 * @param string|null $default
	 * @return mixed|string
	 */
	public function get(string $name, string $default = null) {
		return isset($this->parameters[$name]) ? $this->parameters[$name] : $default;
	}

	/**
	 * @param string $key
	 * @param $value
	 * @return Request
	 */
	public function set(string $key, $value) {
		$this->parameters[$key] = $value;
		return $this;
	}

	/**
	 * @param array $exclude
	 * @return array
	 */
	public function all(array $exclude = []): array {
		return array_filter($this->parameters, function ($value) use ($exclude) {
			return !in_array($value, $exclude);
		}, ARRAY_FILTER_USE_KEY);
	}

	/**
	 * Creates an URI from the given path with the current host, base path and protocol.
	 * Optional parameters will be encoded and appended to the URI.
	 *
	 * @param string $path
	 * @param array $params
	 * @return string
	 */
	public function uri(string $path, array $params = [], bool $absolute = true): string {
		$uri = '';
		if ($absolute) $uri .= $this->baseURL;
		$uri .= new Path($path);
		$firstParam = true;
		foreach ($params as $name => $value) {
			$uri .= ($firstParam ? '?' : '&') . urlencode($name) . '=' . urlencode($value);
			$firstParam = false;
		}
		return $uri;
	}
}