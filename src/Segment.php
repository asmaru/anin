<?php

namespace asmaru\http;

class Segment {

	public $name;

	public $type;

	public $value;

	/** @var array */
	private static $tokens = [
		'T_ANY' => '/^(\*)$/',
		'T_OPTIONAL_VAR' => '/^:([^:\?]+)\?$/',
		'T_VAR' => '/^:([^:]+)$/',
		'T_SUB_PATH' => '/^#([^#]+)$/',
		'T_LITERAL_NOT' => '/^\!(.*)$/',
		'T_LITERAL' => '/^(.*)$/'
	];

	/**
	 * @param array $segments
	 * @return array
	 */
	public static function parse(array $segments) {

		// Get first value
		$name = array_shift($segments);

		// Check all tokens for match
		foreach (self::$tokens as $type => $pattern) {
			if (preg_match($pattern, $name, $matches) === 1) {
				$segment = new self();
				$segment->type = $type;
				$segment->name = $matches[1];
				if (!empty($segments)) return array_merge([$segment], self::parse($segments));
				return [$segment];
			}
		}
	}

	/**
	 * @param $value
	 * @return bool
	 */
	public function match($value) {
		switch ($this->type) {
			case 'T_LITERAL':
				return $this->name === $value;
			case 'T_LITERAL_NOT':
				return $this->name !== $value;
			case 'T_VAR':
				$this->value = $value;
				return !empty($value);
			case 'T_SUB_PATH':
			case 'T_OPTIONAL_VAR':
				$this->value = $value;
				return true;
			case 'T_ANY':
				return true;
			default:
				return false;
		}
	}
}
