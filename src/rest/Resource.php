<?php

namespace asmaru\http\rest;

use asmaru\http\Response;

/**
 * Class Resource
 *
 * @package asmaru\cms\api
 */
interface Resource {

	/**
	 * @param array $data
	 *
	 * @return Response
	 */
	public function post(array $data): Response;

	/**
	 * @param $name
	 * @param array $data
	 *
	 * @return Response
	 */
	public function put(string $name, array $data): Response;

	/**
	 * @param $name
	 *
	 * @return Response
	 */
	public function delete(string $name): Response;

	/**
	 * @param $name
	 *
	 * @return Response
	 */
	public function get(string $name): Response;

	/**
	 * @return Response
	 */
	public function getCollection(): Response;
}