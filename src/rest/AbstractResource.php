<?php

declare(strict_types=1);

namespace asmaru\http\rest;

use asmaru\http\Request;
use asmaru\http\RequestHandler;
use asmaru\http\Response;

abstract class AbstractResource implements RequestHandler, Resource {

	protected Request $request;

	public function __construct(Request $request) {
		$this->request = $request;
	}

	public function execute(): Response {
		if ($this->request->contains(':name')) {
			switch ($this->request->method) {
				case 'post':
					return $this->post($this->request->all([':name']));
				case 'put':
					return $this->put($this->request->get(':name'), $this->request->all([':name']));
				case 'delete':
					return $this->delete($this->request->get(':name'));
				case 'get':
					return $this->get($this->request->get(':name'));
			}
		}

		if ($this->request->is('post')) {
			return $this->post($this->request->all([':name']));
		}

		return $this->getCollection();
	}

	public function post(array $data): Response {
		return $this->createResponse()->status(Response::HTTP_METHOD_NOT_ALLOWED);
	}

	public function put(string $name, array $data): Response {
		return $this->createResponse()->status(Response::HTTP_METHOD_NOT_ALLOWED);
	}

	public function delete(string $name): Response {
		return $this->createResponse()->status(Response::HTTP_METHOD_NOT_ALLOWED);
	}

	public function get(string $name): Response {
		return $this->createResponse()->status(Response::HTTP_METHOD_NOT_ALLOWED);
	}

	public function getCollection(): Response {
		return $this->createResponse()->status(Response::HTTP_METHOD_NOT_ALLOWED);
	}

	protected function createResponse(): Response {
		return new Response();
	}
}