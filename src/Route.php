<?php

namespace asmaru\http;

use InvalidArgumentException;
use function array_slice;
use function implode;
use function sprintf;

class Route {

	public $values = [];

	public $name;

	private $segments;

	public $action;

	private $methods = [];

	/**
	 * @param Path $path
	 * @param $action
	 * @param array $methods
	 * @param $name
	 */
	public function __construct(Path $path, $action, array $methods, $name) {
		$this->segments = Segment::parse($path->toArray());
		$this->action = $action;
		$this->methods = $methods;
		$this->name = $name;
	}

	/**
	 * @param $method
	 * @param Path $path
	 * @return bool
	 */
	public function match($method, Path $path) {
		// Is this method allowed?
		if (!in_array(mb_strtoupper($method), $this->methods)) return false;

		$pathParts = $path->toArray();
		for ($i = 0; $i < count($this->segments); $i++) {
			/** @var Segment $segment */
			$segment = $this->segments[$i];
			$pathPart = isset($pathParts[$i]) ? $pathParts[$i] : null;
			if (!$segment->match($pathPart)) return false;
			if (!empty($segment->value)) {
				if ($segment->type === 'T_SUB_PATH') {
					$this->values[$segment->name] = implode('/', array_slice($pathParts, $i));
				} else {
					$this->values[$segment->name] = $segment->value;
				}
			}
		}
		return true;
	}

	/**
	 * @param array $values
	 * @return string
	 * @throws InvalidArgumentException
	 */
	public function path(array $values = []) {
		$url = [];
		/** @var Segment $segment */
		foreach ($this->segments as $segment) {
			switch ($segment->type) {
				case 'T_LITERAL':
					$url[] = $segment->name;
					break;
				case 'T_VAR':
					if (!isset($values[$segment->name])) throw new InvalidArgumentException(sprintf('Can not create URL for route with name "%s". Missing value for url segment "%s"', $this->name, $segment->name));
					$url[] = $values[$segment->name];
					break;
				case 'T_SUB_PATH':
					if (isset($values[$segment->name])) {
						$url[] = $values[$segment->name];
					}
					break;
				case 'T_OPTIONAL_VAR':
					if (isset($values[$segment->name])) {
						$url[] = $values[$segment->name];
					}
					break;
			}
		}
		return (new Path(implode(Path::PATH_SEPARATOR, $url)))->__toString();
	}
}