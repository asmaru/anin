<?php

declare(strict_types=1);

namespace asmaru\http\error;

use Throwable;

interface HttpError extends Throwable {

	public function getStatus(): int;
}