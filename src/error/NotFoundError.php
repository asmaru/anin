<?php

namespace asmaru\http\error;

use asmaru\http\HttpStatus;
use Exception;

class NotFoundError extends Exception implements HttpError {

	public function getStatus(): int {
		return HttpStatus::HTTP_NOT_FOUND;
	}
}