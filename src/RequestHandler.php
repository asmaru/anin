<?php

declare(strict_types=1);

namespace asmaru\http;

interface RequestHandler {

	public function execute(): ?Response;
}