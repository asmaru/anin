[![Codacy Badge](https://api.codacy.com/project/badge/89c3189a60f34e7eb76f47ca368254b1)](https://www.codacy.com)

Micro HTTP Framework


```
#!php
use Asmaru\Http\App;
use Asmaru\Http\Request;
use Asmaru\Http\Response;

$app = new App();
$app->route('GET /admin/:action/:id?', function (App $app) {
    return (new Response())->write(sprintf('action = %s, id = %s', $app->request->get('action'), $app->request->get('id')));
});
$app->run(Request::build())->out();
```