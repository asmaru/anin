<?php

use asmaru\http\App;
use asmaru\http\Request;
use PHPUnit\Framework\TestCase;

class FilterTest extends TestCase {

	public function testFilter() {
		$app = new App();
		$app->route('* *', function () {
			return true;
		});
		$app->filter('GET admin/*', function ($app) {
			return $app->request->get('key') === 'value';
		});
		$this->assertTrue($app->run(new Request('GET', 'admin/pages/', ['key' => 'value']))->content);
		$this->assertEquals(404, $app->run(new Request('GET', 'admin/pages/'))->status);
		$this->assertTrue($app->run(new Request('GET', 'aaa/'))->content);
	}
}