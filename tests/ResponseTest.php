<?php
namespace asmaru\http;

use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase {

	/**
	 * @runInSeparateProcess
	 */
	public function testRedirect() {
		ob_start();
		$response = new Response();
		$response->redirect('HTTP://example.org');
		$response->send();
		$result = ob_get_clean();
		$this->assertEquals(Response::HTTP_FOUND, http_response_code());
		$this->assertEmpty($result);
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testSetContent() {
		ob_start();
		$response = new Response();
		$response->setContent('<html><head></head><body><h1>test</h1></body></html>', 'text/html');
		$response->send();
		$result = ob_get_clean();
		$this->assertEquals(Response::HTTP_OK, http_response_code());
		$this->assertEquals('<html><head></head><body><h1>test</h1></body></html>', $result);
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testJson() {
		ob_start();
		$response = new Response();
		$response->json(['aa' => 11, 'bb' => 22]);
		$response->send();
		$result = ob_get_clean();
		$this->assertEquals(Response::HTTP_OK, http_response_code());
		$this->assertEquals('{"aa":11,"bb":22}', $result);
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testAbort() {
		ob_start();
		$response = new Response();
		$response->abort();
		$response->send();
		$result = ob_get_clean();
		$this->assertEquals(Response::HTTP_NOT_FOUND, http_response_code());
		$this->assertEmpty($result);
	}
}
