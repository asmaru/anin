<?php
use asmaru\di\ObjectManager;
use asmaru\http\ActionA;
use asmaru\http\App;
use asmaru\http\Request;
use asmaru\http\Response;

class RouteTest extends \PHPUnit\Framework\TestCase {

	public function testLiteralPaths() {
		$expectedResponse = uniqid(true);
		$app = new App(new Request('GET', 'admin/pages/index/'), new ObjectManager());
		$app->route('GET|PUT /admin/pages/index/', function () use ($expectedResponse) {
			return $expectedResponse;
		});
		$this->assertEquals($expectedResponse, $app->run()->content);

		$expectedResponse = uniqid(true);
		$app = new App(new Request('PUT', 'admin/pages/index/'), new ObjectManager());
		$app->route('GET|PUT /admin/pages/index/', function () use ($expectedResponse) {
			return $expectedResponse;
		});
		$this->assertEquals($expectedResponse, $app->run()->content);

		$expectedResponse = uniqid(true);
		$app = new App(new Request('POST', 'admin/pages/index/'), new ObjectManager());
		$app->route('GET|PUT /admin/pages/index/', function () use ($expectedResponse) {
			return $expectedResponse;
		});
		$this->assertEquals(404, $app->run()->status);

		$expectedResponse = uniqid(true);
		$app = new App(new Request('DELETE', 'admin/pages/index/'), new ObjectManager());
		$app->route('GET|PUT /admin/pages/index/', function () use ($expectedResponse) {
			return $expectedResponse;
		});
		$this->assertEquals(404, $app->run()->status);
	}

	public function testRouteWithParams() {
		$app = new App();
		$app->route('GET /admin/pages/:action/:id?', function (App $app) {
			return [
				$app->request->get('action'),
				$app->request->get('id')
			];
		});

		$r = $app->run(new Request('GET', 'admin/pages/index/'));
		$this->assertEquals('index', $r->content[0]);
		$this->assertEquals(null, $r->content[1]);

		$r = $app->run(new Request('GET', 'admin/pages/index/123/'));
		$this->assertEquals('index', $r->content[0]);
		$this->assertEquals('123', $r->content[1]);

		$r = $app->run(new Request('GET', 'admin/pages/'));
		$this->assertEquals(404, $r->status);
	}

	public function testWildard() {
		echo "testWildard\n";
		$app = new App();
		$app->route('GET /admin/*', function () {
			echo "match\n";
			return true;
		});
		$this->assertTrue($app->run(new Request('GET', 'admin/pages/'))->content);
		$this->assertTrue($app->run(new Request('GET', 'admin/pages/index/'))->content);
		$this->assertTrue($app->run(new Request('GET', 'admin/pages/index/123/'))->content);
		$this->assertEquals(404, $app->run(new Request('GET', 'admin/'))->status);
		$this->assertEquals(404, $app->run(new Request('GET', ''))->status);
	}

	public function testDefault() {
		$app = new App();
		$app->route('GET aaa', function () {
			return 'a';
		});
		$app->defaultRoute(function () {
			return (new Response())->json([])->abort(Response::HTTP_NOT_FOUND);
		});
		$this->assertEquals('a', $app->run(new Request('GET', 'aaa'))->content);
		$this->assertEquals(404, $app->run(new Request('GET', 'bbb'))->status);
	}

	public function testNotFound() {
		$app = new App();
		$this->assertEquals(404, $app->run(new Request('GET', 'bbb'))->status);
	}

	public function testActionFromClass() {
		$app = new App();
		$app->defaultRoute(ActionA::class);
		$this->assertEquals('abc', $app->run(new Request('GET', 'aaa'))->content);
	}

	public function testURLFromNamedRoute() {
		$app = new App();
		$app->route('GET admin/pages/:action/:id? as action', function () {
		});
		$this->assertEquals('admin/pages/index', $app->getRoute('action')->path(['action' => 'index']));
		$this->assertEquals('admin/pages/update/123', $app->getRoute('action')->path(['action' => 'update', 'id' => '123']));
	}
}