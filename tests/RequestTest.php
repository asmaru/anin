<?php
namespace asmaru\http;

use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase {

	public function testIs() {
		$request = new Request('GET', new Path(''));
		$this->assertTrue($request->is('GET'));
		$this->assertTrue($request->is('get'));
		$this->assertFalse($request->is('POST'));
	}

	public function testContains() {
		$request = new Request('GET', new Path(''), '', ['a' => '1', 'b' => '2']);
		$this->assertTrue($request->contains('a'));
		$this->assertTrue($request->contains('b'));
		$this->assertFalse($request->contains('c'));
	}

	public function testGet() {
		$request = new Request('GET', new Path(''), '', ['a' => '1', 'b' => '2']);
		$this->assertEquals('1', $request->get('a'));
		$this->assertEquals('2', $request->get('b'));
		$this->assertNull($request->get('c'));
	}

	public function testGetWithDefault() {
		$request = new Request('GET', new Path(''));
		$this->assertFalse($request->contains('a'));
		$this->assertEquals('1', $request->get('a', '1'));
	}

	public function testSet() {
		$request = new Request('GET', new Path(''));
		$request->set('a', '1')->set('b', '2');
		$this->assertEquals('1', $request->get('a'));
		$this->assertEquals('2', $request->get('b'));
	}

	public function testPath() {
		$request = new Request('GET', new Path('aa/bb'));
		$this->assertEquals('aa/bb', $request->path);
	}

	public function testMethod() {
		$request = new Request('GET', new Path(''));
		$this->assertEquals('get', $request->method);
	}

	public function testBaseUrl() {
		$server = [
			'SERVER_PROTOCOL' => 'HTTP/1.1',
			'SERVER_PORT' => '80',
			'SERVER_NAME' => 'example.org'
		];
		$request = new Request('GET', new Path('/aaa/bbb.html'), '', [], $server);
		$this->assertEquals('aaa/bbb.html', $request->path);
		$this->assertEquals('http://example.org', $request->baseURL);
		$this->assertFalse($request->isHTTPS());
	}

	public function testBaseUrlHttps() {
		$server = [
			'SERVER_PROTOCOL' => 'HTTP/1.1',
			'SERVER_PORT' => '443',
			'SERVER_NAME' => 'example.org',
			'HTTPS' => 'on'
		];
		$request = new Request('GET', new Path('/aaa/bbb.html'), '', [], $server);
		$this->assertEquals('aaa/bbb.html', $request->path);
		$this->assertEquals('https://example.org', $request->baseURL);
		$this->assertTrue($request->isHTTPS());
	}

	public function testCanCreateURIWithParams() {
		$request = new Request('GET', new Path(''));
		$expected = 'http://aaa/bbb?c%26c=d+%3F+d&eee=1';
		$result = $request->uri('aaa/bbb', ['c&c' => 'd ? d', 'eee' => true]);
		$this->assertEquals($expected, $result);
	}
}
