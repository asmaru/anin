<?php
namespace asmaru\http;

use PHPUnit\Framework\TestCase;

class PathTest extends TestCase {

	public function testNormalize() {
		$this->assertEquals('aa/bb/cc', (new Path('aa/bb/cc'))->__toString());
		$this->assertEquals('aa/bb/cc', (new Path('/aa/bb/cc'))->__toString());
		$this->assertEquals('aa/bb/cc', (new Path('aa/bb/cc/'))->__toString());
		$this->assertEquals('aa/bb/cc', (new Path('/aa/bb/cc/'))->__toString());
	}

	public function testToArray() {
		$result = (new Path('aa'))->toArray();
		$this->assertCount(1, $result);
		$this->assertEquals('aa', $result[0]);

		$result = (new Path('aa/bb'))->toArray();
		$this->assertCount(2, $result);
		$this->assertEquals('aa', $result[0]);
		$this->assertEquals('bb', $result[1]);

		$result = (new Path('aa/bb/cc'))->toArray();
		$this->assertCount(3, $result);
		$this->assertEquals('aa', $result[0]);
		$this->assertEquals('bb', $result[1]);
		$this->assertEquals('cc', $result[2]);
	}
}
