<?php
namespace asmaru\http;

class ActionA implements RequestHandler {

	/**
	 * @param App $app
	 * @return Response
	 * @internal param Request $request
	 */
	public function execute(App $app) {
		return 'abc';
	}
}