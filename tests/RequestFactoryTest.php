<?php
namespace asmaru\http;

use PHPUnit\Framework\TestCase;

class RequestFactoryTest extends TestCase {

	public function testNormalizeFiles() {
		$_FILES = [
			'userfile1' => [
				'name' => '1.jpg',
				'type' => 'image/jpeg',
				'tmp_name' => '1temp',
				'error' => 0,
				'size' => 1036763
			],
			'userfile2' => [
				'name' => '2.jpg',
				'type' => 'image/jpeg',
				'tmp_name' => '2temp',
				'error' => 0,
				'size' => 1036763
			],
			'userfile3' => [
				'name' => [
					'3.jpg',
					'4.jpg'
				],
				'type' => [
					'image/jpeg',
					'image/jpeg'
				],
				'tmp_name' => [
					'3temp',
					'4temp'
				],
				'error' => [
					0, 0
				],
				'size' => [
					1036763,
					1036763
				]
			]
		];
		$expected = [
			'userfile1' => [
				[
					'name' => '1.jpg',
					'type' => 'image/jpeg',
					'tmp_name' => '1temp',
					'error' => 0,
					'size' => 1036763
				]
			],
			'userfile2' => [
				[
					'name' => '2.jpg',
					'type' => 'image/jpeg',
					'tmp_name' => '2temp',
					'error' => 0,
					'size' => 1036763
				]
			],
			'userfile3' => [
				[
					'name' => '3.jpg',
					'type' => 'image/jpeg',
					'tmp_name' => '3temp',
					'error' => 0,
					'size' => 1036763
				],
				[
					'name' => '4.jpg',
					'type' => 'image/jpeg',
					'tmp_name' => '4temp',
					'error' => 0,
					'size' => 1036763
				]
			]
		];

		$_SERVER['REQUEST_METHOD'] = 'POST';
		$_SERVER['BASEPATH'] = '';

		$factory = new RequestFactory();
		$request = $factory->build();

		$this->assertEquals($expected, $request->files());
	}
}
